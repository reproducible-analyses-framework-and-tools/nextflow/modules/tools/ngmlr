#!/usr/bin/env nextflow

process ngmlr {
// Runs ngmlr
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   REF
//   params.minimap2$minimap2_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'ngmlr_container'
  label 'ngmlr'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  path fa
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.sam'), optional: true, emit: sams

  script:
  """
  ngmlr ${parstr} -r ${fa} -q ${fq} -t ${task.cpus} -o ${dataset}-${pat_name}-${run}.sam
  """
}
